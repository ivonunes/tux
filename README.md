# Tux - Universal Package Manager Wrapper


## What is Tux?

Tux is my attempt at making an easy to use and feature complete wrapper for various packages managers.


## Why should I use Tux?

You maybe be tired of seeing things like universal package managers or wrappers like Tux. To me this opinion is just that, an opinion. There are many of us *nix users out there either beginner or advanced who enjoy testing different *nix distributions on a regular basis and would probably enjoy having something to fill in the gaps of package managers. Of course while it is important to know how to use the package manager on your current system, sometimes you may not like typing out the syntax (some package managers have long and tedious syntax) every time you want to do simple mundane tasks. Tux aims to allow you to use those same commands but with a much easier syntax. Thus making your task quicker and your stress level just a little bit lower.


## Why the name "Tux"?  

Since Tux is the Linux mascot, I decided it was the perfect generic name and symbol for something like a universal package manager (or wrapper). Tux represents Linux as a whole in symbolic form. The idea here is that the mascot is giving all package managers a great big hug and tying them all together. Pretty cheesy, but I like it. 

## Dependencies  

* python

## Features

* Install packages (remote repository only)
* Re-install packages (remote repository only)
* Remove packages
* Sync cache from remote repository
* Update installed packages
* Search remote repository
* Clean package cache


## Examples 

### Install a package  

``$ tux i firefox``  
``$ tux install firefox``  

### Re-install a package  

``$ tux ri firefox``  
``$ tux reinstall firefox``  

### Remove a package  

``$ tux r firefox``  
``$ tux remove firefox``  

### Upgrade packages 

``$ tux u``  
``$ tux upgrade``  

### Sync package cache  

``$ tux sy``  
``$ tux sync``  
``$ tux -Sy``  

### Clean package cache  

``$ tux c``  
``$ tux clean`` 

## Contributing  

If you like the idea of Tux and/or know how to create better software than me, feel free to clone this repository and create of pull request of your changes. You can also contribute by sharing this project with your friends and other website across the internet.  


## Closing  

Thank you for using Tux, I am very happy to work on this project and discover new knowledge along the way. I hope you enjoy this project and find it beneficial as much as I do.  

- Silvernode




