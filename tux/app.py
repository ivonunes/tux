import argparse
import sys
import pkgutil


class App():
    def __init__(self):
        self.providers = []
        abbreviations = {
            'i': 'install',
            'r': 'remove',
            'u': 'upgrade',
            'sy': 'sync',
            'c': 'clean'
        }

        parser = argparse.ArgumentParser(
            description='Universal Package Manager Wrapper',
            usage='''tux <command> [<args>]

The most commonly used tux commands are:
   install     Install a package
   remove      Remove a package
   upgrade     Upgrade all installed packages
   search      Search the database for a package
   sync        Sync the package manager database
   clean       Clean the package cache
''')
        parser.add_argument('command', help='Command to run')

        args = parser.parse_args(sys.argv[1:2])

        if args.command in abbreviations:
            args.command = abbreviations[args.command]

        if not hasattr(self, args.command):
            print 'Unrecognized command'
            parser.print_help()
            exit(1)

        for provider in [name for _, name,
                         _ in pkgutil.iter_modules(['tux/providers'])]:
            p = __import__(
                'tux.providers.' + provider,
                fromlist=['']
            ).Provider

            if p.available():
                self.providers.append(p())

        getattr(self, args.command)()

    def clean(self):
        for provider in self.providers:
            provider.clean()

    def install(self):
        parser = argparse.ArgumentParser(
            description='Install a package')

        parser.add_argument('--force', '-f', action='store_true')
        parser.add_argument('packages', nargs='+')
        args = parser.parse_args(sys.argv[2:])

        for provider in self.providers:
            if provider.install(args.packages, args.force):
                return

    def remove(self):
        parser = argparse.ArgumentParser(
            description='Remove a package')

        parser.add_argument('--force', '-f', action='store_true')
        parser.add_argument('packages', nargs='+')
        args = parser.parse_args(sys.argv[2:])

        for provider in self.providers:
            if provider.remove(args.packages, args.force):
                return

    def search(self):
        parser = argparse.ArgumentParser(
            description='Search for a package')

        parser.add_argument('package')
        args = parser.parse_args(sys.argv[2:])

        for provider in self.providers:
            provider.search(args.package)

    def sync(self):
        for provider in self.providers:
            provider.sync()

    def upgrade(self):
        parser = argparse.ArgumentParser(
            description='Upgrade packages')

        parser.add_argument('--force', '-f', action='store_true')

        args = parser.parse_args(sys.argv[2:])

        for provider in self.providers:
            provider.upgrade(args.force)
