class Provider():
    @staticmethod
    def available():
        return False

    def __init__(self):
        pass

    def clean(self):
        print 'Cleaning cache'

    def install(self, packages, force=False):
        print 'Installing %s' % packages

    def remove(self, packages, force=False):
        print 'Removing %s' % packages

    def search(self, package):
        print 'Searching for %s' % package

    def sync(self):
        print 'Syncing'

    def upgrade(self, force=False):
        print 'Running upgrade, force=%s' % force
