import os
from tux.utils import which


class Provider():
    @staticmethod
    def available():
        return which('brew')

    def __init__(self):
        pass

    def clean(self):
        os.system('brew cleanup')

    def install(self, packages, force=False):
        os.system('brew install ' + ' '.join(packages))

    def remove(self, packages, force=False):
        os.system('brew uninstall ' + ' '.join(packages))

    def search(self, package):
        os.system('brew search ' + package)

    def sync(self):
        os.system('brew update')

    def upgrade(self, force=False):
        os.system('brew update && brew upgrade')
